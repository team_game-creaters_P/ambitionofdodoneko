// Status.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.
// 160927 v01

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

namespace Mijinko
{

	[System.Serializable]
	/// <summary>
	/// ステータス管理の神
	/// </summary>
	public struct Status
	{
		// *** 変数 ***
		// 実質の値を保存してる変数
		[SerializeField]
		private float status;
		[SerializeField]
		private float status_add;
		[SerializeField]
		private float status_multip;

		/// 値の取得(読み取り専用)
		public float value
		{
			get { return (status + status_add) * status_multip; }
			// get { return status * status_multip + status_add; }
		}


		// *** コンストラクタ ***
		public Status (float val)
		{
			status = val;
			status_add = 0;
			status_multip = 1;
		}


		// *** メソッド ***
		/// 基本値を入力。返り値は総合値
		public float SetStatus (float val) { status = val; return value; }
		
		/// 追加値を入力。返り値は総合値
		public float SetStatusAdd (float val) { status_add = val; return value; }
		
		/// 乗算値を入力。返り値は総合値
		public float SetStatusMultip (float val) { status_multip = val; return value; }

		/// 追加値を追加。返り値は総合値。演算(Status + float)でも同じ
//		public float AddStatusAdd (float val) { status_add += val; return value; }
		
		/// 乗算値を追加。返り値は総合値。演算(Status * float)でも同じ
//		public float AddStatusMultip (float val) { status_multip += val; return value; }

		public override string ToString () { return status +" +"+ status_add +" *"+ status_multip; }


		// *** 演算 ***
		public static float operator + (Status a, float b)
		{
			a.status_add += b;
			return a.value;
		}
		public static float operator - (Status a, float b)
		{
			a.status_add -= b;
			return a.value;
		}
		public static float operator * (Status a, float b)
		{
			a.status_multip += b;
			return a.value;
		}
		public static float operator / (Status a, float b)
		{
			a.status_multip -= b;
			return a.value;
		}

		public static bool operator == (Status a, Status b)
		{ // 一緒かどうか
			return (Mathf.Abs(a.value - b.value) < 9.999999E-11);
		}
		public static bool operator == (Status a, float b)
		{ // 一緒かどうか
			return (Mathf.Abs(a.value - b) < 9.999999E-11);
		}
		public static bool operator == (float a, Status b)
		{ // 一緒かどうか
			return (Mathf.Abs(a - b.value) < 9.999999E-11);
		}
		public static bool operator != (Status a, Status b)
		{ // 違うかどうか
			return (Mathf.Abs(a.value - b.value) >= 9.999999E-11);
		}
		public static bool operator != (Status a, float b)
		{ // 違うかどうか
			return (Mathf.Abs(a.value - b) >= 9.999999E-11);
		}
		public static bool operator != (float a, Status b)
		{ // 違うかどうか
			return (Mathf.Abs(a - b.value) >= 9.999999E-11);
		}

		public static float operator + (float a, Status b)
		{
			return a + b.value;
		}
		public static float operator - (float a, Status b)
		{
			return a - b.value;
		}
		public static float operator * (float a, Status b)
		{
			return a * b.value;
		}
		public static float operator / (float a, Status b)
		{
			return a / b.value;
		}
		public static float operator % (float a, Status b)
		{
			return a % b.value;
		}

	}

}