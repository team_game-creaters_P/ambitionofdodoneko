// MinMax.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.
// 160927 v03

using UnityEngine;
// using UnityEngine.UI;
using System.Collections;
// using System.Collections.Generic;

namespace Mijinko
{

	[System.Serializable]
	/// <summary>
	/// 最大値と最小値を司る神
	/// </summary>
	public struct MinMax
	{
		// *** 変数 ***
		// 実質の値を保存してる変数
		[SerializeField]
		private float _min;
		[SerializeField]
		private float _max;

		// 最大値と最小値やけど、お互いを超える値が入った時は値を入れ替える
		public float Min
		{
			get { return _min; }
			set {
				_min = value;
				if (_min > _max) {
					_min = _max;
					_max = value;
				}
			}
		}
		public float Max
		{
			get { return _max; }
			set {
				_max = value;
				if (_min > _max) {
					_max = _min;
					_min = value;
				}
			}
		}


		// *** コンストラクタ ***
		// public MinMax () { Min = Max = 0; }
		public MinMax (float val) {
			_min = 0; _max = val;
			if (_min > _max) { _min = val; _max = 0; }
		}
		public MinMax (float min, float max) {
			_min = min; _max = max;
			if (_min > _max) { _min = max; _max = min; }
		}


		/// 差を出す
		public float Gap { get { return Max - Min; } }

		/// 平均値
		public float Center { get { return Min + (Max - Min)/2; } }


		/// 最小値以上、最大値より小さいランダムな値を出す
		public float RandomBetoween () { return Min + UnityEngine.Random.value * (Max - Min); }

		/// 最小値以上、最大値より小さいかどうかを判定する
		public bool IsBetoween (float val) { return (Min <= val & Max > val); }

		/// 範囲内に矯正する
		public float BeBetoween (float val) { return Mathf.Min(Max, Mathf.Max(Min, val)); }


		// *** 演算 ***
		public static MinMax operator + (MinMax a, MinMax b)
		{ // 最大値最小値をそのまま足す
			return new MinMax (a.Min + b.Min, a.Max + b.Max);
		}
		public static MinMax operator - (MinMax a, MinMax b)
		{ // 最大値最小値をそのまま引く
			return new MinMax (a.Min - b.Min, a.Max - b.Max);
		}

		public static bool operator == (MinMax a, MinMax b)
		{ // 一緒かどうか
			return (Mathf.Abs(a.Min - b.Min) < 9.999999E-11 & Mathf.Abs(a.Max - b.Max) < 9.999999E-11);
		}
		public static bool operator != (MinMax a, MinMax b)
		{ // 違うかどうか
			return (Mathf.Abs(a.Min - b.Min) >= 9.999999E-11 | Mathf.Abs(a.Max - b.Max) >= 9.999999E-11);
		}

		public static MinMax operator + (MinMax a, float b)
		{ // 最大値最小値それぞれに足す
			return new MinMax (a.Min + b, a.Max + b);
		}
		public static MinMax operator - (MinMax a, float b)
		{ // 最大値最小値それぞれに引く
			return new MinMax (a.Min - b, a.Max - b);
		}
		public static MinMax operator * (MinMax a, float b)
		{ // 最大値最小値それぞれに掛ける
			return new MinMax (a.Min * b, a.Max * b);
		}
		public static MinMax operator / (MinMax a, float b)
		{ // 最大値最小値それぞれに割る
			return new MinMax (a.Min / b, a.Max / b);
		}

		public static MinMax operator - (MinMax a)
		{ // 最大値最小値それぞれを負にする
			return new MinMax (-a.Max, -a.Min);
		}

		public override string ToString () { return Min.ToString() +" - "+ Max.ToString(); }

	}

}