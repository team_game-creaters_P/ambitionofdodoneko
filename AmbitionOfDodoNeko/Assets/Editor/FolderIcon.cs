﻿// FolderIcon.cs
// copyright Kazuki_FUKUNAGA 2016 un-Pro.
// 参考：http://baba-s.hatenablog.com/entry/2015/05/07/103743

using UnityEditor;
using UnityEngine;

public static class FolderIcon
{
    [InitializeOnLoadMethod]
    private static void OnLoad()
    {
        EditorApplication.projectWindowItemOnGUI += OnGUI;
    }
    
    private static void OnGUI( string guid, Rect selectionRect )
    {
        string path = AssetDatabase.GUIDToAssetPath( guid );

        string fileName = path.Split('/')[path.Split('/').Length -1];
        Texture texture;
        if (texture = Resources.Load<Texture>("FolderIcon/" + fileName))
        {
            Rect pos = selectionRect;
            if (pos.height > 16) pos.height -= 16;
            if (pos.height > 64) {
                pos.x += (pos.height - 64)/2;
                pos.y += (pos.height - 64)/2;
                pos.height = 64;
            }
            pos.width = pos.height;
	        GUI.DrawTexture(pos, texture);
        }
    }
}